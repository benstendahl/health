<?php

namespace Config;

use Database\PDO\DB;
use Database\Mongo\MDB;
use Logging\Log;
use Exceptions\ConfigException;

/**
 * Config handles the loading of configuration values within ini 
 * files and the retrieval of those values statically. The class 
 * provides the benefit of a single instantiation as well as
 * throwing an exception when trying to retrieve an undeclared
 * configuration value.
 *
 * This is a singleton:
 * <pre>
 *  try {
 *		$my_config = Config\Config::get('my_config');
 *	} catch (ConfigException $e) {
 *		// attempting to get an unknown configuration
 * 	}
 * </pre>
 *
 * @package Config
 * @author	Ben Stendahl
 */
class Config
{
	/**
	 * @static
	 * @var	String
	 */
	protected static $configDir = "cfg/";

	/**
	 * @static
	 * @var	array
	 */
	protected static $configs = array();

	/**
	 * @static
	 * @var	boolean
	 */
	protected static $init = FALSE;


	/**
	 * Reads the contents of the init files into the static configs
	 * array for retrieval through the static::get() method
	 *
	 * @static
	 * @throws	Exception
	 * @return	boolean
	 */
	public static function init()
	{
		// we only init once
		if (static::$init == FALSE) {
			static::$init = TRUE;
			static::$configDir = __DIR__ . "/../../" . static::$configDir;
			$files = scandir(static::$configDir);
		
			// looping in reverse allows us to unset without ksort 
			// and an additional declaration of $count=count($files).
			for ($i=(count($files)-1); $i>=0; $i--) {
				// Ignoring files with a "." prefix, hidden.
				if (strpos($files[$i], ".") === 0) {
					unset($files[$i]);
					continue;
				}
				
				// loop and load all .ini files
				$parts = explode(".", $files[$i]);
				$ext = array_pop($parts);
				if (strtolower($ext) == "ini") {
					$config = parse_ini_file(static::$configDir . $files[$i]);
					if (count(array_intersect_key($config, static::$configs)) > 0) {
						// throw Exception with colliding declarations
						$intersect = array_intersect_key($config, static::$configs);
						throw new ConfigException("Configuration key collision: " . print_r($interset, TRUE));
					} else {
						// merge into the static configuration array 
						static::$configs = array_merge(static::$configs, $config);
					}
				} else {
					unset($files[$i]);
					continue;
				}
			}
		} 
		// all keys to lower case
		static::$configs = array_change_key_case(static::$configs, CASE_LOWER);

		static::initDB();
		static::initLog();
		return static::$init;
	}


	public static function initDB()
	{
		if (static::$init) {
			$keys = array_keys(static::$configs);
			// the convention is that database connection keys are named
			// in the format db.<name>.<config>
			for ($i=0; $i<count($keys); $i++) {
				$pieces = explode(".", $keys[$i]);
				if ($pieces[0] == "db") {
					$db[$pieces[1]][$pieces[2]] = static::$configs[$keys[$i]];
				}
			}

			// register a connection for each database configuration
			foreach ($db as $key=>$val) {
				if (isset($val['dsn'], $val['username'], $val['password'])) {
					DB::config($key, array(
						'dsn' => $val['dsn'], 
						'username' => $val['username'], 
						'password' => $val['password']
					));
				} else {
					throw new ConfigException("Missing a configuration for database connection [$key]");
				}
			}
		} else {
			static::init();
		}
	}
	
	public static function initMDB()
	{
		if (static::$init) {
			// TODO load any log configurations
			// the conventions is that log keys are name
			// in the format log.<name>.<config>
				
		} else {
			static::init();
			static::initLog();
		}
	}
	
	
	public static function initLog()
	{
		if (static::$init) {
			$keys = array_keys(static::$configs);
			// the convention is that database connection keys are named
			// in the format log.<name>.<config>
			for ($i=0; $i<count($keys); $i++) {
				$pieces = explode(".", $keys[$i]);
				if ($pieces[0] == "log") {
					$log[$pieces[1]][$pieces[2]] = static::$configs[$keys[$i]];
				}
			}
			
			// register a connection for each log configuration
			foreach ($log as $key=>$val) {
				if (isset($val['directory'], $val['filename'], $val['verbosity'])) {
					Log::config($key, array(
						'directory' => $val['directory'], 
						'fileName' => $val['filename'], 
						'verbosity' => $val['verbosity']
					));
				} else {
					throw new ConfigException("Missing a configuration for log [$key]");
				}
			}
		} else {
			static::init();
		}
	}


	/**
	 * Return the configuration value requested by the given key.
	 *
	 * @static
	 * @param	String
	 * @throws	Exception
	 * @return	mixed
	 */
	public static function get($key)
	{
		// check that it's been initialized
		if (static::$init == FALSE) {
			static::init();
		}

		$key = strtolower($key);
		// fetch the configuration if it exists
		if (array_key_exists($key, static::$configs)) {
			return static::$configs[$key];
		} else {
			throw new ConfigException("Configuration key [$key] does not exist");
		}
	}

	/**
	 * Set a runtime configuration. Use caution as this will overwrite
	 * any current configurations set with the provided key
	 * 
	 * @param String
	 * @param mixed
	 */
	public static function set($key, $val)
	{
		static::$configs[$key] = $val;
	}
}
