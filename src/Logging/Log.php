<?php

namespace Logging;

use Logging\LogInterface;
use Exceptions\ConfigException;
use Exceptions\FileException;

/**
 * 
 * 
 * @author benstendahl
 *
 */
class Log implements LogInterface
{

	const EMERGENCY = 1;
	const ALERT		= 2;
	const CRITICAL	= 4;
	const ERROR		= 8;
	const WARNING	= 16;
	const NOTICE	= 32;
	const INFO		= 64;
	const DEBUG		= 128;
	
	/**
	 * 2 dimensional array for storing instantiated instances
	 * of Log classes for singleton retrieval 
	 * 
	 * @var array
	 */
	protected static $instance = array();
	
	/**
	 * 2 dimensional array for storing configuration entries
	 * 
	 * @var array	(name)(directory, fileName, verbosity)
	 */
	protected static $config = array();
	
	/**
	 * Set this to prepend all subsequent lines with this value.
	 * This exists for ALL instances and configurations of
	 * this class.
	 * 
	 * @var unknown
	 */
	public static $indicator;
	
	
	
	protected $message;
	
	protected $level;
	
	
	
	protected $name;
	
	protected $directory;
	
	protected $fileName;
	
	protected $verbosity;
	
	protected $filePath;
	
	protected $fileStream;
	
	
	
	/**
	 * Set a configuration for later use as a singleton call to 
	 * the Log::open() method. The primary configuration should
	 * be named 'default' by convention.  
	 * 
	 * @param String
	 * @param array 	(directory, fileName, verbosity)
	 * @throws ConfigException
	 * @return null
	 */
	public static function config($name, array $parameters)
	{
		if (isset(static::$config[$name])) {
			throw new ConfigException("Log configuration [$name] already registered");
		}
		static::$config[$name] = $parameters;
	}
	
	/**
	 * Grab an open connection or instantiate a configured Log
	 * from the static::$config array and return that. A 
	 * ConfigException is thrown if no configuration for the 
	 * parameter is found. A FileException is thrown if the class 
	 * is unable to open a stream for that configuration. 
	 * 
	 * The default value for the parameter is 'default' which 
	 * is the same default parameter for each of the static
	 * shortcut methods. ex. Log::emergency
	 * 
	 * @param String
	 * @throws ConfigException
	 * @throws FileException
	 * @return \Logging\Log
	 */
	public static function open($name = 'default')
	{
		// If we already have it, return it
		if (isset(static::$instance[$name])) {
			return static::$instance[$name];
		}
		
		// If it hasn't been configured, throw an error
		if (! isset(static::$config[$name])) {
			throw new ConfigException("Log configuration [$name] has not been registered");
		}
		
		// If we don't have it but it's configured, build it
		static::$instance[$name] = new \Logging\Log($name, static::$config[$name]);
		return static::$instance[$name];
	}
	
	/**
	 * Constructor, opens the file handle
	 * 
	 * @param String
	 * @param array 	(directory, fileName, verbosity)
	 * @throws FileException
	 */
	public function __construct($name, array $config) 
	{
		$this->name = $name;
		$this->verbosity = $config['verbosity'];
		$this->fileName = $config['fileName'];
		$this->directory = $config['directory'];
		$this->filePath = $this->directory . $this->fileName . "." . date("md");
		$this->fileStream = fopen($this->filePath, "a");
		if ($this->fileStream === FALSE) {
			throw new FileException("Unable to open [" . $this->filePath . "] for writing");
		}
	}
	
	/**
	 * Destructor, closes the file handle
	 */
	public function __destruct()
	{
		fclose($this->fileStream);
	}
	
	/**
	 * Perform the write to the open file stream created in 
	 * the construct
	 * 
	 * @param mixed		String, array, object
	 * @param Log::<const>
	 * @return boolean
	 */
	public function write($message, $level = self::INFO)
	{
		if ($level & $this->verbosity) {
			$this->level = $level;
			$this->message = $this->formatMessage($message);
			if (fwrite($this->fileStream, $this->message) !== FALSE) {
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/**
	 * Format the parameter into the format needed for 
	 * 
	 * @param mixed		String, array, object
	 * @return String
	 */
	protected function formatMessage($message)
	{
		// do we have a line indicator?
		$indicator = (! empty(static::$indicator)) ? static::$indicator : '';
		
		// for non-string messages
		if (is_array($message) || is_object($message)) {
			$message = print_r($message, TRUE);
		}
		
		// format the message, per the rules
		$formatted = "[" . date("Y-m-d H:i:s") . "] ";
		$formatted .= $this->getLevelString() . " ";
		$formatted .= (! empty($indicator)) ? $indicator . " - " : "- ";
		$formatted .= str_replace(array(
				"\r", 
				"\t", 
				"\n"
		), " ", trim($message));
		$formatted .= PHP_EOL;
		return $formatted;
	}
	
	/**
	 * Format the integer leve Log::<const> into the necessary string
	 * version for insertion into the log line.
	 * 
	 * @return string
	 */
	protected function getLevelString()
	{
		switch ($this->level) {
			case static::EMERGENCY:
				return "[emergency]";
			case static::ALERT:
				return "[alert]";
			case static::CRITICAL:
				return "[critical]";
			case static::ERROR:
				return "[error]";
			case static::WARNING:
				return "[warning]";
			case static::NOTICE:
				return "[notice]";
			case static::INFO:
				return "[info]";
			case static::DEBUG:
				return "[debug]";
			default:
				return "";
		}
	}
	
	/**
	 * Write an emergency log message 
	 * 
	 * @param String 
	 * @param string 	Optional, name of log configuration
	 * @return boolean
	 */
	public static function emergency($message, $name = 'default')
	{
		$log = static::open($name);
		return $log->write($message, static::EMERGENCY);
	}
	
	/**
	 * Write an alert log message
	 *
	 * @param String
	 * @param string 	Optional, name of log configuration
	 * @return boolean
	 */
	public static function alert($message, $name = 'default')
	{
		$log = static::open($name);
		return $log->write($message, static::ALERT);
	}
	
	/**
	 * Write an critical log message
	 *
	 * @param String
	 * @param string 	Optional, name of log configuration
	 * @return boolean
	 */
	public static function critical($message, $name = 'default')
	{
		$log = static::open($name);
		return $log->write($message, static::CRITICAL);
	}
	
	/**
	 * Write an error log message
	 *
	 * @param String
	 * @param string 	Optional, name of log configuration
	 * @return boolean
	 */
	public static function error($message, $name = 'default')
	{
		$log = static::open($name);
		return $log->write($message, static::ERROR);
	}
	
	/**
	 * Write an warning log message
	 *
	 * @param String
	 * @param string 	Optional, name of log configuration
	 * @return boolean
	 */
	public static function warning($message, $name = 'default')
	{
		$log = static::open($name);
		return $log->write($message, static::WARNING);
	}
	
	/**
	 * Write an notice log message
	 *
	 * @param String
	 * @param string 	Optional, name of log configuration
	 * @return boolean
	 */
	public static function notice($message, $name = 'default')
	{
		$log = static::open($name);
		return $log->write($message, static::NOTICE);
	}
	
	/**
	 * Write an info log message
	 *
	 * @param String
	 * @param string 	Optional, name of log configuration
	 * @return boolean
	 */
	public static function info($message, $name = 'default')
	{
		$log = static::open($name);
		return $log->write($message, static::INFO);
	}
	
	/**
	 * Write an debug log message
	 *
	 * @param String
	 * @param string 	Optional, name of log configuration
	 * @return boolean
	 */
	public static function debug($message, $name = 'default')
	{
		$log = static::open($name);
		return $log->write($message, static::DEBUG);
	}
	
}
