<?php

namespace Logging;

interface LogInterface
{
	public static function emergency($message);

	public static function alert($message);

	public static function critical($message);

	public static function error($message);

	public static function warning($message);

	public static function notice($message);

	public static function info($message);

	public static function debug($message);
}
