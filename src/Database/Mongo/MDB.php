<?php

namespace Database\Mongo;

use Exceptions\DatabaseException;

class MDB extends \MongoClient
{

	/**
	 * Array of instantiated, configured connections
	 * @static
	 * @var	array	(name, Database\Mongo\MDB)
	 */
	protected static $instance = array();

	/**
	 * Configuration array for a named connection
	 * @static
	 * @var	array	(name)(server,(options))
	 */
	protected static $config = array();

	/**
	 * Register a configuration to be used as a singleton throughout
	 * the rest of the applicatoin. It is recommended that the 
	 * primary connection be specified with the 'name' parameter being
	 * set as 'default so that the calls to Database\Mongo\MDB::connect()
	 * can omit the 'name' parameter.
	 *
	 * @static
	 * @param	String	The name of the connection
	 * @param	array 	Mongo options
	 * @return	null
	 */
	public static function config($name, array $mongoParameters) 
	{
		if (isset(static::$config[$name])) {
			throw new DatabaseException("Mongo configuration [$name] already registered");
		}
		static::$config[$name] = $mongoParameters;
	}

	/**
	 * Grab an already created instance of a MongoClient using a
	 * configured coneection details as specified by 'name' in 
	 * Database\Mongo\MDB::config(name, options). If the configuration 
	 * exists but has not yet been instantiated then an instance will 
	 * be created and returned. If no configuration is found for the 
	 * 'name' parameter supplied then an Exception is thrown.
	 *
	 * @static
	 * @param	String	The name of the connection to use
	 * @throws	Exception
	 * @return	Database\Mongo\MDB
	 */
	public static function open($name = 'default')
	{
		if (isset(static::$instance[$name])) {
			return static::$instance[$name];
		} else if (isset(static::$config[$name])) {
			// try to connect with the configured options if applicable
			try {
				if (isset(static::$config[$name]['options'])) {
					$mongo = new \Database\Mongo\MongoDB(
						static::$config[$name][$server], 
						static::$config[$name][$options]
					);
				} else {
					$mongo = new \Database\Mongo\MongoDB(
						static::$config[$name]
					);
				}
			} catch (\MongoConnectionException $e) {
				// chain this exception for a little more context
				throw new DatabaseException(
					"Unable to connect to Mongo with [$name] configuration", 
					$e->getCode(),
					$e
				);
			}
			static::$instance[$name] = $mongo;
			return static::$instance[$name];
		}
		throw new DatabaseException("No configuration [$name] registered");
	}

}
