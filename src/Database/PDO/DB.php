<?php

namespace Database\PDO;

use Exceptions\DatabaseException;
use Logging\Log;

/**
 * Singleton or extended PDO class.
 *
 * Usage:
 * <pre>
 *  // Configured somewhere in the bootstrap
 *	Database\PDO\DB::config('default', array(
 *		'dsn' => 'your:dsn:info', 
 *		'username' => 'db_user', 
 *		'password' => 'db_user_pass');
 *
 *	// Usage in the code
 *	$pdo = Database\PDO\DB::connect();
 *	$statement = $pdo->query($sql); // familiar PDO usage
 * </pre>
 *
 * @package	Database
 * @author	Ben Stendahl
 */
class DB extends \PDO
{

	/** 
	 * Static array of instantiated, configured connections
	 * @static
	 * @var	array	(name, Database\ODBC\DB}
	 */
	protected static $instance = array();

	/**
	 * Configuration array for a named connection 
	 * @static
	 * @var	array	(name)(dsn, username, password, options)
	 */
	protected static $config = array();

	/**
	 * Register a configuration to be used as a singleton througout
	 * the rest of the application. It is recommended that the 
	 * primary connection be specified with the 'name' parameter being
	 * set as 'default' so that calls to Database\ODBC\DB::connect() can 
	 * omit the 'name' parameter.
	 *
	 * @static
	 * @param	String	The name of the connection
	 * @param	array	(dsn, username, password, [options])
	 * @return 	null
	 */
	public static function config($name, array $pdoParameters)
	{
		if (isset(static::$config[$name])) {
			throw new DatabaseException("Database configuration [$name] already registered"); 
		}
		static::$config[$name] = $pdoParameters;
	}

	/**
	 * Grab an already created instance of a PDO using configured 
	 * connection details as specified by 'name' in 
	 * Database\ODBC\DB::config(name, parameters). If the configuration 
	 * exists but has not yet been instantiated then an instance will 
	 * be created and returned. If no configuration is found for the 
	 * 'name' parameter supplied then an Exception is thrown.
	 *
	 * @static
	 * @param	String
	 * @throws	Exceptions\DatabaseException
	 * @return	Database\ODBC\DB
	 */
	public static function open($name = 'default')
	{
		if (isset(static::$instance[$name])) {
			return static::$instance[$name];
		} else if (isset(static::$config[$name])) {
			if (isset(static::$config[$name]['options'])) {
				$pdo = new \Database\PDO\DB(
							static::$config[$name]['dsn'], 
							static::$config[$name]['username'], 
							static::$config[$name]['password'], 
							static::$config[$name]['options']);
			} else {
				$pdo = new \Database\PDO\DB(
							static::$config[$name]['dsn'], 
							static::$config[$name]['username'], 
							static::$config[$name]['password']);
			}
			static::$instance[$name] = $pdo;
			return static::$instance[$name];
		}
		throw new DatabaseException("No configuration [$name] registered");
	}

	/**
	 * Creates a PDO instance representing a connection to a database 
	 * using the extended Statement class for further visibility and 
	 * customization.
	 * 
	 * @param	String
	 * @param	String
	 * @param	String
	 * @param 	array
	 * @return	mixed	Database\DB on success or FALSE on failure
	 */
	public function __construct($dsn, $username, $password, $options = array())
	{
		try {
			parent::__construct($dsn, $username, $password, $options);
			$this->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array('\Database\PDO\Statement'));
		} catch (\PDOException $e) {
			Log::emergency($e->getMessage());
			return FALSE;
		}
	}

	/**
	 * Prepares a Database\Statement for execution and returns that
	 * object. Logs statement information.
	 *
	 * @param	String
	 * @param	array
	 * @return	Database\Statement
	 */
	public function prepare($sql, $driverOptions = array())
	{
		if (count($driverOptions) > 0) {
			$statement = parent::prepare($sql, $driverOptions);
		} else {
			$statement = parent::prepare($sql);
		}

		Log::debug("prepare: " . $sql);

		return $statement;
	}

	/**
	 * Executes an SQL statement. Logs query information.
	 *
	 * @param	String
	 * @param	int		\PDO::<fetch_type>
	 * @return	Database\Statement
	 */
	public function query($sql, $fetchType = null) 
	{
		if ($fetch_type != null) {
			$statement = parent::query($sql, $fetchType);
		} else {
			$statement = parent::query($sql);
		}

		Log::debug("query: " . $sql);

		return $statement;
	}

}
