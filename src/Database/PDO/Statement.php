<?php

namespace Database\PDO;

use Logging\Log;

class Statement extends \PDOStatement
{

	/**
	 * Binds a parameter to a specified variable name
	 *
	 * @param	String
	 * @param	mixed	
	 * @param	int	
	 * @param	int
	 * @param	mixed
	 * @return	boolean
	 */
	public function bindParam($parameter, &$variable, $dataType = null, $length = null, $driverOptions = null)
	{
		if ($driver_options != null) {
			$ret = parent::bindParam($parameter, $variable, $dataType, $length, $driverOptions);
			$params = array(
					'parameter' => $parameter, 
					'variable' => $variable, 
					'dataType' => $dataType, 
					'length' => $length, 
					'driverOptions' => $driverOptions
			);
		} else if ($length != null) {
			$ret = parent::bindParam($parameter, $variable, $dataType, $length);
			$params = array(
					'parameter' => $parameter,
					'variable' => $variable,
					'dataType' => $dataType,
					'length' => $length
			);
		} else if ($data_type = null) {
			$ret = parent::bindParam($parameter, $variable, $dataType);
			$params = array(
					'parameter' => $parameter,
					'variable' => $variable,
					'dataType' => $dataType
			);
		} else {
			$ret = parent::bindParam($parameter, $variable);
			$params = array(
					'parameter' => $parameter,
					'variable' => $variable
			);
		}
		
		if ($ret) {
			Log::debug("bindParam: " . print_r($params, TRUE));
		} else {
			Log::error("unable to bindParam: " . print_r($params, TRUE));
		}
		
		return $ret;
	}

	/**
	 * Binds a value to a parameter
	 *
	 * @param	String
	 * @param	mixed
	 * @param	int		/PDO::PARAM_<constants>
	 * @return	boolean
	 */
	public function bindValue($parameter, $value, $dataType = null)
	{
		if ($data_type != null) {
			$ret = parent::bindValue($parameter, $value, $dataType);
			$params = array(
					'parameter' => $parameter, 
					'value' => $value, 
					'dataType' => $dataType
			);
		} else {
			$ret = parent::bindValue($parameter, $value);
			$params = array(
					'parameter' => $parameter,
					'value' => $value
			);
		}

		if ($ret) {
			Log::debug("bindValue: " . print_r($params, TRUE));
		} else {
			Log::error("unable to bindValue: " . print_r($params, TRUE));
		}

		return $ret;
	}

	/**
	 * Executes a prepared statement
	 *
	 * @param	array
	 * @returns	boolean
	 */
	public function execute($inputParameters = array())
	{
		if (count($inputParameters) > 0) {
			$ret = parent::execute($inputParameters);
		} else {
			$ret = parent::execute();
		}

		$message = "";
		if (empty($inputParameters)) {
			$message = " inputParameters: " . print_r($inputParameters, TRUE);
		}
		
		if ($ret && ! empty($message)) {
			Log::debug("executed prepared statement with" . $message);
		} else if (! $ret && ! empty($message)) {
			Log::error("unable to execute prepared statement with" . $message);
		} else if (! $ret) {
			Log::error("unable to execute prepared statement");
		}

		return $ret;
	}

}
