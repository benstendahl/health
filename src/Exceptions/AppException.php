<?php

namespace Exceptions;

class AppException extends \Exception
{
	
	public function __construct($message = null, $code = 0, \Exception $previous = null) 
	{
		// TODO create a log entry

		if (is_null($previous) !== FALSE) {
			parent::__construct($message, $code, $previous);
		} else if ($code != 0) {
			parent::__construct($message, $code);
		} else if (is_null($message) !== FALSE) {
			parent::__construct($message);
		} else {
			parent::__construct();
		}
	}

}
