<?php

class Autoload
{

	public static function init()
	{
		spl_autoload_register('Autoload::namespaceAutoload');
	}

	public static function namespaceAutoload($class)
	{
		$dirs = explode('\\', $class);
		$srcPath = str_replace('\\', DIRECTORY_SEPARATOR, $class);
		$path = __DIR__ . DIRECTORY_SEPARATOR . $srcPath . '.php';
		if (file_exists($path)) {
			require $path;
		} else {
			throw new \Exception("No class [$class] found at path [$path]");
		}
	}
}

Autoload::init();
