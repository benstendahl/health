<?php

require 'src/Autoload.php';

// pull in any configurations
Config\Config::init();

// require any Composer dependencies
if (file_exists('vendor/autoload.php')) {
	require 'vendor/autoload.php';
} else {
	// Nothing, since it's only needed for PHPUnit in dev
}
