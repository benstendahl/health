<?php
	require '../bootstrap.php';
	
	use Config\Config;
	use Exceptions\ConfigException;
	
	class ConfigTest extends PHPUnit_Framework_TestCase
	{
		
		public function testConfigGet()
		{
			// valid configuration value
			$val = Config::get('db.default.name');
			$this->assertEquals('default', $val);
			
			// invalied configuration value
			try {
				$val = Config::get('does.not.exist');
				$this->assertNotNull($val, "This value should be null and exception should have been thrown");
			} catch (ConfigException $e) {
				$this->assertNotEmpty($e->getMessage(), "Message set properly");
			}
		}

		public function testConfigSet()
		{
			// configuration values
			$key = "test.key";
			$val = "some value";

			// set, get the value
			Config::set($key, $val);
			$result = Config::get($key);
			
			$this->assertEquals($val, $result);
		}
	}
?>
