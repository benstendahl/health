<?php

	require '../bootstrap.php';
	
	use Logging\Log;
	
	class LogTest extends PHPUnit_Framework_TestCase
	{
		public $key;
		public $config = array();
		public $className;
		
		public function setUp()
		{
			$this->key = "test";
			$this->config = array(
				'directory' => '/tmp/',
				'fileName' => 'test.log',
				'verbosity' => 255
			);
			$this->className = 'Logging\Log';
		}
		
		public function testConfig()
		{
			Log::config($this->key, $this->config);
			$log = Log::open($this->key);
			$this->assertEquals($this->className, get_class($log));
		}
		
		public function testWrite()
		{
			$log = Log::open($this->key);
			$message = "Test writing the 'info' log entry";
			$val = $log->write($message, Log::INFO);
			$this->assertTrue($val, "Wrote to the log successfully");
		}
		
	}

?>