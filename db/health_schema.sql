-- CREATE DATABASE IF NOT EXISTS health;

USE health;

CREATE TABLE application_status (
	`id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
	`name` VARCHAR(32) NOT NULL, 
	`description` VARCHAR(255) NOT NULL, 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT 'The different statuses an application can report';

CREATE TABLE application (
	`id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
	`name` VARCHAR(64) NOT NULL, 
	`active` TINYINT(4) NOT NULL DEFAULT 1,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT 'List of applications that have checked in';

CREATE TABLE heartbeat (
	`id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
	`application_id` INT(11) unsigned  NOT NULL, 
	`source` VARCHAR(255) NULL COMMENT 'IP, DNS or some other identifier of source location', 
	`application_status_id` INT(11) unsigned NULL,
	`created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
	PRIMARY KEY (`id`), 
	FOREIGN KEY (`application_id`) 
		REFERENCES `application`(`id`)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	FOREIGN KEY (`application_status_id`)
		REFERENCES `application_status`(`id`)
		ON UPDATE CASCADE
		ON DELETE RESTRICT	
) ENGINE=InnoDB COMMENT 'Record of each heartbeat received';


-- Severity
-- 0 = Good, 1 = Outside of Normal, 2 = Try to connect to source, 3 = Outage
-- 0 = Green, 1 = Yellow, 2 = Orange, 3 = Red
CREATE TABLE health_status (
	`id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
	`name` VARCHAR(32) NOT NULL,
	`description` VARCHAR(255) NOT NULL, 
	`severity` INT(11) NOT NULL COMMENT 'The severity determines if we attempt to connect to the source or not', 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT 'The different statuses the the health application can report';

CREATE TABLE health (
	`id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
	`application_id` INT(11) unsigned NOT NULL, 
	`health_status_id` INT(11) unsigned NOT NULL,
	`last_heartbeat` TIMESTAMP NULL, 
	`updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
		ON UPDATE CURRENT_TIMESTAMP, 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT 'The current health reported for the application';
