USE health;


-- --------------------
--  Health Statuses --
-- --------------------
INSERT INTO health_status (
	name, 
	description, 
	severity
) VALUES (
	'healthy', 
	'The application, server or utility is checking in normally and appears healthy', 
	'0'
);

INSERT INTO health_status (
	name, 
	description, 
	severity
) VALUES (
	'notice', 
	'The application, server or utility is just outside its normal check in time', 
	'1'
);

INSERT INTO health_status (
	name, 
	description, 
	severity
) VALUES (
	'warning', 
	'The application, server or utility has missed a check in', 
	'1'
);

INSERT INTO health_status (
	name, 
	description, 
	severity
) VALUES (
	'severe', 
	'The application, server or utility is outside its threshold', 
	'2'
);

INSERT INTO health_status (
	name, 
	description, 
	severity
) VALUES (
	'failure', 
	'The application, server or utility is no longer checking in', 
	'3'
);

INSERT INTO health_status (
	name, 
	description, 
	severity
) VALUES (
	'unresponsive', 
	'The application, server or utility is not responding to outbound requests', 
	'3'
);


-- ------------------------
-- Application Statuses --
-- ------------------------
INSERT INTO application_status (
	name, 
	description
) VALUES (
	'up', 
	'Service is up and operating normally'
);

INSERT INTO application_status (
	name, 
	description
) VALUES (
	'maintenance', 
	'Service is undergoing maintenance'
);

INSERT INTO application_status (
	name, 
	description
) VALUES (
	'down', 
	'Service is down and not able to operate normally'
);

INSERT INTO application_status (
	name, 
	description
) VALUES (
	'degraded', 
	'Service is degraded from normal operating quality'
);
